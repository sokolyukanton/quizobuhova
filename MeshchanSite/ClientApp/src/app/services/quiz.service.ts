import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuizData } from '../models';

@Injectable()
export class QuizService {
  constructor (private http: HttpClient) {

  }

  getQuestions(): QuizData[] {
    return [
      {
        "questionNumber": "1.1",
        "questionText": "Хто на Ваш погляг більше опікується (повинен опікуватися) станом навкольшнього середовища",
        "options": [
          {
            "class": "A",
            "optionText": "Пересічне місцеве населення",
            "now": 50,
            "desired": 50
          },
          {
            "class": "C",
            "optionText": "Місцева влада",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "1.2",
        "questionText": "Хто на Ваш погляг більше опікується (повинен опікуватися) станом навкольшнього середовища",
        "options": [
          {
            "class": "B",
            "optionText": "Громадські екологічні організації",
            "now": 50,
            "desired": 50
          },
          {
            "class": "D",
            "optionText": "Питання повинні вирішуватися на державному рівні",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "2.1",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану повітря?",
        "options": [
          {
            "class": "A",
            "optionText": "Забруднення повітря небкзпечними сполуками, що входять до складу аерозолів",
            "now": 50,
            "desired": 50
          },
          {
            "class": "C",
            "optionText": "Забруднення повітря викидами вуглицю від транспорту",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "2.2",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану повітря?",
        "options": [
          {
            "class": "B",
            "optionText": "Збільшення забруднень повітря внаслідок зменьшення площі лісів та інших зелених насаджень",
            "now": 50,
            "desired": 50
          },
          {
            "class": "D",
            "optionText": "Забруднення повітря шкідливими викидами промисловості",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "3.1",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану водоймів?",
        "options": [
          {
            "class": "A",
            "optionText": "Засмічення озер та річок побутовими відходами",
            "now": 50,
            "desired": 50
          },
          {
            "class": "C",
            "optionText": "Відсутність або недостатня кількість планових заходів по очищенню водоймів місцевого значення",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "3.2",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану водоймів?",
        "options": [
          {
            "class": "B",
            "optionText": "Недостадній контроль за епідеолонічним станом водойм",
            "now": 50,
            "desired": 50
          },
          {
            "class": "D",
            "optionText": "Забруднення річок та морів відходами промисловості",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "4.1",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану землі (грунтів)?",
        "options": [
          {
            "class": "A",
            "optionText": "Несортування побутових відходів",
            "now": 50,
            "desired": 50
          },
          {
            "class": "C",
            "optionText": "Розростання площ зайнятих звалищами побутових і непобутових відходів",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "4.2",
        "questionText": "Які проблеми Ви вважаєте найнебезпечнішими для стану землі (грунтів)?",
        "options": [
          {
            "class": "B",
            "optionText": "Недостання кількість підприємств по перепобці віходів",
            "now": 50,
            "desired": 50
          },
          {
            "class": "D",
            "optionText": "Захоронення дуже небезпечних радіоактивних та токсичних відходів на території України",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "5.1",
        "questionText": "Які проблеми Ви ввжаете найнебезпечнішими для стану флори та фауни?",
        "options": [
          {
            "class": "A",
            "optionText": "Браконьерство",
            "now": 50,
            "desired": 50
          },
          {
            "class": "C",
            "optionText": "Вирубка лісів та осушення водоймів",
            "now": 50,
            "desired": 50
          }
        ]
      },
      {
        "questionNumber": "5.2",
        "questionText": "Які проблеми Ви ввжаете найнебезпечнішими для стану флори та фауни?",
        "options": [
          {
            "class": "B",
            "optionText": "Недостатній контроль над знищенням природних ареалів розселення рослин та тварин",
            "now": 50,
            "desired": 50
          },
          {
            "class": "D",
            "optionText": " Охорона розлин та тварин занесених у червону книги України на законодавчому рівні",
            "now": 50,
            "desired": 50
          }
        ]
      }
    ];
  }
}
