import { Component, ViewChild } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { ChartData, UpdateRecord } from './models';
import { SelectItem } from 'primeng/api';
import {QuizTableComponent} from "./quiz-table/quiz-table.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('quiztable') quiztable: QuizTableComponent;
  public chartData: ChartData;
  public ages: SelectItem[] = [
    { label: '3-18', value: 1 },
    { label: '19-30', value: 1.2 },
    { label: '31-60', value: 1.3 },
    { label: '61-100', value: 1.4 }
  ];
  public selectedAge = 1;
  public roles: SelectItem[] = [
    { label: 'Студент', value: 1 },
    { label: 'Викладач', value: 1.5 }
  ];
  public selectedRole = 1;
  
  public options= {
    scale: {
        display: true,
		ticks: {
        min: 0,
        max: 100,
        stepSize: 5
      },
    }
};

  constructor(private http: HttpClient) {
    this.chartData = {
      labels: ['A - Заходи по охороні навколишнього середовища на особистісному рівні',
               'B - Заходи по охороні навколишнього середовища на місцевому рівні',
               'C - Заходи по охороні навколишнього середовища на рівні громадських екологічних організації',
               'D - Заходи по охороні навколишнього середовища на загальнодержавному рівні'],
      datasets: [
        {
          label: 'Зараз',
          backgroundColor: 'rgba(179,181,198,0.2)',
          borderColor: 'rgba(179,181,198,1)',
          pointBackgroundColor: 'rgba(179,181,198,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(179,181,198,1)',
          data: [50, 50, 50, 50]
        },
        {
          label: 'Бажано',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          pointBackgroundColor: 'rgba(255,99,132,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(255,99,132,1)',
          data: [50, 50, 50, 50]
        }
      ]
    };
  }

  onSaveClick() {
    this.quiztable.updateAverageValues();
    let body = {
      role: this.selectedRole,
      age: this.selectedAge,
      now: this.quiztable.averageValues.now,
      desired: this.quiztable.averageValues.desired
    }
    console.log(body);
    this.http.post("/api/quiz/answer", body).subscribe(() => {},error=>console.log(error));
  }

  onDropClick() {
    this.http.delete("/api/quiz/drop").subscribe(() => { }, error => console.log(error));
  }

  async onTabChange(event:any) {
    if (event.index === 1) {
      var av:any=await this.http.get("/api/quiz/averages").toPromise();
      const data: ChartData = { labels: this.chartData.labels, datasets: this.chartData.datasets };
      data.datasets[0].data = av.now;
      data.datasets[1].data = av.desired;
      this.chartData = data;
    }
  }
}
