import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { QuizData, UpdateRecord } from '../models';
import { QuizOption } from '../models/quiz-data';
import { QuizService } from './../services/quiz.service';

@Component({
  selector: 'app-quiz-table',
  templateUrl: './quiz-table.component.html',
  styleUrls: ['./quiz-table.component.css']
})
export class QuizTableComponent implements OnInit {

  public quiz: QuizData[];

  public averageValues: UpdateRecord = {
    now: {},
    desired: {}
  };

  constructor(private quizService: QuizService) {
  }

  async ngOnInit() {
    this.quiz = this.quizService.getQuestions();
    this.updateAverageValues();
  }

  onSliderValueChanged(options: QuizOption[], className: string, label: 'now' | 'desired', value: number) {
    var optionToUpdate = options.find(o => o.class !== className);
    optionToUpdate[label] = 100 - value;
  }

  public updateAverageValues() {
    this.quiz.forEach(question => {
      this.averageValues.now[question.options[0].class] = 0;
      this.averageValues.now[question.options[1].class] = 0;
      this.averageValues.desired[question.options[0].class] = 0;
      this.averageValues.desired[question.options[1].class] = 0;
    });

    this.quiz.forEach(question => {
      this.averageValues.now[question.options[0].class] += question.options[0].now;
      this.averageValues.now[question.options[1].class] += question.options[1].now;
      this.averageValues.desired[question.options[0].class] += question.options[0].desired;
      this.averageValues.desired[question.options[1].class] += question.options[1].desired;
    });

    Object.getOwnPropertyNames(this.averageValues.now).forEach(className => {
      this.averageValues.now[className] /= this.getClassCount(className);
    });
    Object.getOwnPropertyNames(this.averageValues.desired).forEach(className => {
      this.averageValues.desired[className] /= this.getClassCount(className);
    });
  }

  private getClassCount(className: string) {
    return this.quiz.reduce((prev: number, curr) => 
      prev + curr.options.reduce((prevNum: number, currOptn) => prevNum + +(currOptn.class === className), 0),
    0);
  }
}
