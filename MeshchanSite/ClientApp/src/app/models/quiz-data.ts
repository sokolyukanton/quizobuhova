export class QuizData {
  questionNumber: string;
  questionText: string;
  options: QuizOption[];
}

export class QuizOption {
  class: string;
  optionText: string;
  now: number;
  desired: number;
}
