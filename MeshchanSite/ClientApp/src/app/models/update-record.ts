export class UpdateRecord {
  now: {
   [className: string]: number 
  };
  desired: {
    [className: string]: number
  }
}
