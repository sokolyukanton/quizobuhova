import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { ChartModule } from 'primeng/chart';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';

import { AppComponent } from './app.component';
import { QuizTableComponent } from './quiz-table/quiz-table.component';

import { QuizService } from './services/quiz.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    TableModule,
    SliderModule,
    ChartModule,
    TabViewModule,
    DropdownModule,
    ButtonModule
  ],
  declarations: [
    AppComponent,
    QuizTableComponent
  ],
  providers: [QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }
