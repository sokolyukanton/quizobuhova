﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using MeshchanSite.Models;
using Microsoft.AspNetCore.Mvc;

namespace MeshchanSite.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : ControllerBase
    {
        private DataContext _context = new DataContext();

        [HttpPost("answer")]
        public ActionResult SaveAnswer(Answer answer)
        {
            _context.AddAnswer(answer);
            return Ok();
        }

        [HttpGet("averages")]
        public ActionResult<TotalAverage> GetTotalAverage()
        {
            return Ok(_context.GetTotalAverages());
        }

        [HttpDelete("drop")]
        public ActionResult DropAnswers()
        {
            _context.DropAnswers();
            return Ok();
        }
    }
}