﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace MeshchanSite.Models
{
    public class Answer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "role")]
        public double RoleMultiplier { get; set; }

        [JsonProperty(PropertyName = "age")]
        public double AgeMultiplier { get; set; }

        [JsonProperty(PropertyName = "desired")]
        public OptionsValues Desired { get; set; }

        [JsonProperty(PropertyName = "now")]
        public OptionsValues Now { get; set; }
    }

    public class OptionsValues
    {
        public double A { get; set; }

        public double B { get; set; }

        public double C { get; set; }

        public double D { get; set; }
    }
}
