﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace MeshchanSite.Models
{
    public class TotalAverage
    {
        public double[] now { get; set; }
        public double[] desired { get; set; }
    }

    public class DataContext
    {
        private readonly IMongoDatabase _db;

        public IMongoCollection<Answer> Answers => _db.GetCollection<Answer>("Answers");
        public DataContext()
        {
            var client = new MongoClient("mongodb://user:user@cluster0-shard-00-00-1ispn.azure.mongodb.net:27017,cluster0-shard-00-01-1ispn.azure.mongodb.net:27017,cluster0-shard-00-02-1ispn.azure.mongodb.net:27017/" +
                                         "db?retryWrites=true&ssl=true&authSource=admin");
            _db = client.GetDatabase("db");
        }

        public void AddAnswer(Answer answer)
        {
            Answers.InsertOne(answer);
        }

        public TotalAverage GetTotalAverages()
        {
            var avNowValues = new []
            {
                Answers.AsQueryable().Average(answer => answer.Now.A * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Now.B * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Now.C * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Now.D * answer.AgeMultiplier * answer.RoleMultiplier)
            };
            var avDesiredValues = new []
            {
                Answers.AsQueryable().Average(answer => answer.Desired.A * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Desired.B * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Desired.C * answer.AgeMultiplier * answer.RoleMultiplier),
                Answers.AsQueryable().Average(answer => answer.Desired.D * answer.AgeMultiplier * answer.RoleMultiplier)
            };
            return new TotalAverage {now = avNowValues, desired = avDesiredValues};
        }

        public void DropAnswers()
        {
            _db.DropCollection("Answers");
            AddAnswer(new Answer()
            {
                AgeMultiplier = 1,
                RoleMultiplier = 1,
                Desired = new OptionsValues{A = 50,B=50,C=50,D=50},
                Now = new OptionsValues{A = 50,B=50,C=50,D=50},
            });
        }
    }
}
